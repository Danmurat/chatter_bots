import os
import time
import nltk
from nltk import word_tokenize, pos_tag
import pdb #pdb.set_trace()

deviceType = raw_input("What is your Amazon/Google wake word/sentence: ")
startTopic = raw_input("Give me a random word to start talking about: ")

def defineType(theWord, sentence):

	wordMatch = pos_tag(word_tokenize(sentence)) #tokenizes whole sentence into noun, verb, adj, etc

	for sublist in wordMatch: #compares the chosen word against the wordMatch list to get the wordType
    		if sublist[0] == theWord:
        		wordType = sublist[1]
        	else:
        		pass

	extension=""
	ext=""
	genericAnsEnd = "A word of this type makes it hard to continue our conversation! Lets talk about something else!"
	
	if wordType.startswith('V'):
		extension = "a verb"
		ext = "What is the meaning of, "
	elif wordType.startswith('N'):
		extension = "a noun"
		ext = "What is the meaning of, "
	elif wordType.startswith('R'):
		extension = "an adverb. "
		ext = "What is the meaning of, "
	elif wordType.startswith('J'):
		extension = "an adjective. "
		ext = "What is the meaning of, "
	elif wordType.startswith('P'):
		extension = "a pronoun. " + genericAnsEnd
	elif wordType.startswith(('D','W')):
		extension = "a determiner. " + genericAnsEnd
	elif wordType.startswith('I'):
		extension = "a preposition. " + genericAnsEnd
	elif wordType.startswith('U'):
		extension = "a interjection. " + genericAnsEnd
	else:
		extension = genericAnsEnd

	if len(theWord) < 3:
		extension = "far too short to talk about! Let's ask to talk about something else!"
	else:
		pass
	
	return extension, ext

def main(startTopicChat):
	os.system('say "' + deviceType + '"')
	time.sleep(1)
	if startTopic.strip()[-1] == "s":
		os.system('say "What are ' + startTopic + '"')
		print("MAC: What are " + startTopic)
	else:
		os.system('say "What is a ' + startTopic + '"')
		print("MAC: What is a " + startTopic)

	ansRandom = False
	
	while True:
		import speech_recognition as sr
		r = sr.Recognizer()
		r.pause_threshold = 2
		r.dynamic_energy_threshold = False
		
		with sr.Microphone(sample_rate = 9000) as source:
			r.adjust_for_ambient_noise(source)
			audio = r.listen(source)
			
		try:
			answer = r.recognize_google(audio, language = "en-GB", show_all=False) #need to cut down response or it times out!!
			print("ALEXA: " + answer)
			sentence = answer

			if answer.startswith("your random"):
				splitAnswer = answer.rsplit(' ')
				theWord = splitAnswer[4]
			else:	
				splitAnswer = answer.rsplit(' ', 1)
				theWord = splitAnswer[1]
		
			returnedDefinition = defineType(theWord, sentence) #('verb', 'Whats')
			#print(returnedDefinition)

			if ansRandom == True:
				os.system('say "The random word is, ' + theWord + ' and its a ' +  returnedDefinition[0] +'"')
				print("MAC: The random word is, " + theWord + " and its a " +  returnedDefinition[0])
			else:
				os.system('say "' + theWord + ' is the last word you mentioned, and its ' +  returnedDefinition[0] +'"')
				print("MAC: " + theWord + " is the last word you mentioned, and its " +  returnedDefinition[0])
				if returnedDefinition[0].endswith("something else!"):
					ansRandom = True
				else:
					ansRandom = False

			time.sleep(1)
			os.system('say "' + deviceType + '"')
			time.sleep(1)
			if ansRandom == True:
				os.system('say "Ask random word skill for a word to start a new discussion"')
				print("MAC: Ask random word skill for a word to start a new discussion")
				ansRandom = False
			else:
				os.system('say "' + returnedDefinition[1] + theWord + '"')
				print("MAC: " + returnedDefinition[1] + theWord)

		except LookupError:
		    print("Could not understand audio")

main(startTopic)